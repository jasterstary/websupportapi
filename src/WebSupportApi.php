<?php

namespace JasterStary\WebSupportApi;

class WebSupportApi{
  
  public function __construct($cfg = false){
    $this->endpoint = "https://rest.websupport.sk";
    
    $this->apiKey = "";
    $this->secret = "";
    
    if (is_array($cfg)) {
      if ((isset($cfg['secret']))&&(is_string($cfg['secret']))) {
        $this->secret = $cfg['secret'];
      }
      if ((isset($cfg['apikey']))&&(is_string($cfg['apikey']))) {
        $this->apiKey = $cfg['apikey'];
      }
    }
  }

  public function getUser() {
    return self::getJSONResponse('/user');
  }

  public function getMe() {
    return self::getJSONResponse('/user/self');
  }

  public function getServices() {
    return self::getJSONResponse('/user/self/service');
  }

  public function getService($id) {
    return self::getJSONResponse('/user/self/service/'.intval($id));
  }
  
  public function getHostings() {
    return self::getJSONResponse('/user/self/hosting');
  }

  public function getHosting($id) {
    return self::getJSONResponse('/user/self/hosting/'.intval($id));
  }

  public function getHostingSize($id) {
    return self::getJSONResponse('/user/self/hosting/'.intval($id).'/size-stats');
  }

  public function getHostingDomainsUsage($id) {
    return self::getJSONResponse('/user/self/hosting/'.intval($id).'/domain-stats');
  }

  public function getHostingFtpSize($id) {
    return self::getJSONResponse('/user/self/hosting/'.intval($id).'/ftp-stats');
  }

  public function getHostingDatabaseUsers($id) {
    return self::getJSONResponse('/user/self/hosting/'.intval($id).'/dbusers');
  }

  public function getHostingDatabases($id) {
    return self::getJSONResponse('/user/self/hosting/'.intval($id).'/db');
  }

  public function getHostingDatabase($idhosting,$iddatabase) {
    return self::getJSONResponse('/user/self/hosting/'.intval($idhosting).'/db/'.intval($iddatabase));
  }

  public function getHostingDatabaseSize($idhosting,$iddatabase,$interval='',$length=1) {
    $q='';
    if (in_array($interval,['day','hour'])) {
      $q='?interval='.$interval.'&length='.intval($length);
    };
    return self::getJSONResponse('/user/self/hosting/'.intval($idhosting).'/db/'.intval($iddatabase).'/size-stats',$q);
  }

  public function getHostingMailBoxes($id) {
    return self::getJSONResponse('/user/self/hosting/'.intval($id).'/mailbox');
  }

  public function getHostingMailBox($idhosting,$idmailbox) {
    return self::getJSONResponse('/user/self/hosting/'.intval($idhosting).'/mailbox/'.intval($idmailbox).'');
  }

  public function getHostingMailBoxSize($idhosting,$idmailbox,$interval='',$length=1) {
    $q='';
    if (in_array($interval,['day','hour'])) {
      $q='?interval='.$interval.'&length='.intval($length);
    };
    return self::getJSONResponse('/user/self/hosting/'.intval($idhosting).'/mailbox/'.intval($idmailbox).'/size-stats',$q);
  }

  public function getHostingMailBoxesSize($idhosting,$interval='',$length=1) {
    $q='';
    if (in_array($interval,['day','hour'])) {
      $q='?interval='.$interval.'&length='.intval($length);
    };
    return self::getJSONResponse('/user/self/hosting/'.intval($idhosting).'/mail/size-stats',$q);
  }

  public function getHostingDomainMailBoxesSize($idhosting,$iddomain,$interval='',$length=1) {
    $q='';
    if (in_array($interval,['day','hour'])) {
      $q='?interval='.$interval.'&length='.intval($length);
    };
    return self::getJSONResponse('/self/hosting/'.intval($idhosting).'/mail/'.intval($iddomain).'/size-stats',$q);
  }

  public function getHostingFtpAccounts($id) {
    return self::getJSONResponse('/user/self/hosting/'.intval($id).'/ftp-account');
  }

  public function getHostingFtpAccount($idhosting,$idaccount) {
    return self::getJSONResponse('/user/self/hosting/'.intval($idhosting).'/ftp-account/'.intval($idaccount));
  }
  
  public function getHostingVhosts($id) {
    return self::getJSONResponse('/user/self/hosting/'.intval($id).'/vhost');
  }

  public function getHostingVhost($idhosting,$idvhost) {
    return self::getJSONResponse('/user/self/hosting/'.intval($idhosting).'/vhost/'.intval($idvhost));
  }

  public function getDomainProfile() {
    return self::getJSONResponse('/user/self/domainProfile');
  }

  public function getAvailableServices($market='sk') {
    if (!in_array($market,['sk','cz','hu','at'])) return [];
    $path = '/order/'.$market;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, sprintf('%s%s%s', $this->endpoint, '/v1', $path));
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($ch);
    curl_close($ch);
    return json_decode($response,true);   
  }

  public function validateDomain($domain, $market='sk') {
    if (!in_array($market,['sk','cz','hu','at'])) return [];
    $post = [
      'domain' => $domain,
    ];
    $path = '/order/'.$market.'/validate/domain';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, sprintf('%s%s%s', $this->endpoint, '/v1', $path));
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));
    $response = curl_exec($ch);
    curl_close($ch);
    return json_decode($response,true);   
  }
  
  public function orderDomain($domain, $noteToHelpDesk='') {
    $me = $this->getMe();
    if (!is_array($me)) return ['error'=>'me is not found'];
    if (!isset($me['id'])) return ['error'=>'my ID is not found'];
    return self::getJSONResponse('/user/'.intval($me['id']).'/order',[
      'services'=>[
        [
          'domain' => $domain,
          'type' => 'domain',
        ],
      ],
      'note' => $noteToHelpDesk,
    ]);
  }
  
  public function getResponse($path,$query=''){  
    $time = time();
    $method = 'GET';
    
    $path = '/v1'.$path;

    $ch = curl_init();
    if (is_array($query)) {
      $method = 'POST';
      curl_setopt($ch, CURLOPT_URL, sprintf('%s%s', $this->endpoint, $path));
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($query));
    } else {
      curl_setopt($ch, CURLOPT_URL, sprintf('%s%s%s', $this->endpoint, $path, $query));
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    }
    $canonicalRequest = sprintf('%s %s %s', $method, $path, $time);
    $signature = hash_hmac('sha1', $canonicalRequest, $this->secret);
    
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, $this->apiKey.':'.$signature);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
      'Date: ' . gmdate('Ymd\THis\Z', $time),
    ]);
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
  }
  
  public function getJSONResponse($path,$query=''){
    return json_decode(self::getResponse($path,$query),true);
  }

}


