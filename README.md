# WebSupport Api

Reading api of WebSupport - web hosting provider.

Just another trivial API reader.

For me to test Gitlab -> Packagist workflow.

## Installation

```

composer require jasterstary/websupportapi


```

## Config


```


return [
  "apikey" => 'get-your-own-api-key',
  "secret" => 'get-your-own-secret',
];



```

You can obtain your API key here (must to be logged in):
[https://admin.websupport.sk/sk/auth/apiKey](https://admin.websupport.sk/sk/auth/apiKey)


## Usage


```php

use JasterStary\WebSupportApi\WebSupportApi as WebSupportApi;

$wsa = new WebSupportApi(config('websupport'));

$wsa->getUser();

$wsa->getServices();

$wsa->getService($id_service);

$wsa->getHostings();

$wsa->getHosting($id_hosting);

$wsa->getHostingSize($id_hosting);

$wsa->getHostingDomainsUsage($id_hosting);

$wsa->getHostingFtpSize($id_hosting);

$wsa->getHostingDatabaseUsers($id_hosting);

$wsa->getHostingDatabases($id_hosting);

$wsa->getHostingDatabase($id_hosting,$id_database);

$interval = 'day'; $length = 14;

$wsa->getHostingDatabaseSize($id_hosting,$id_database,$interval,$length);

$wsa->getHostingMailBoxes($id_hosting);

$wsa->getHostingMailBox($id_hosting,$id_mailbox);

$wsa->getHostingMailBoxSize($id_hosting,$id_mailbox,$interval,$length);

$wsa->getHostingMailBoxesSize($id_hosting,$interval,$length);

$wsa->getHostingDomainMailBoxesSize($id_hosting,$id_domain,$interval,$length);

$wsa->getHostingFtpAccounts($id_hosting);

$wsa->getHostingFtpAccount($id_hosting,$id_account);
  
$wsa->getHostingVhosts($id_hosting);

$wsa->getHostingVhost($id_hosting,$id_vhost);

$wsa->getDomainProfile();

$wsa->getAvailableServices('sk');

$wsa->validateDomain($domain, 'sk');
  
$wsa->orderDomain($domain, $noteToHelpDesk);


```

## Cache

It is wise to use cache, so you are not bothering api server with each request.

In case of Laravel, this is how to do it:


```php

  Cache::remember('HostingMailBoxesSize', 3600,
  function()use(&$wsa) {
    return $wsa->getHostingMailBoxesSize(654321,'day',14);
  })


```



## License
MIT License.

